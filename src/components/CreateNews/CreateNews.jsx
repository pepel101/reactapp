import React from 'react';
import './CreateNews.css';

const authors = [
    "Egor",
    "Ahmed-ibn-kaktotam",
    "Noname"
]

class CreateNews extends React.Component {

    constructor(props) {
        super(props)



        this.state = {
            newsTitle: "",
            newsText: "",
            id: 0,
            author: authors[0]
        }
        this.setNewsTitle = this.setNewsTitle.bind(this);
        this.addNewPost = this.addNewPost.bind(this);
        this.setAuthor = this.setAuthor.bind(this);
        this.setNewsText = this.setNewsText.bind(this);
    }

    setNewsTitle(event) {  //значения из поля ввода заголовка 
        event.preventDefault();
        this.setState({
            newsTitle: event.target.value,
         
        });
    }

    setNewsText(event) { //значения из поля ввода текста 
        event.preventDefault();
        this.setState({
            newsText: event.target.value,
        });
    }


    setAuthor(event) { //значения из поля ввода автора
        event.preventDefault();
        this.setState({
            author: event.target.value
        })
    }




    addNewPost() {  // создание нового поста
       
        const id = ++this.state.id;
        this.setState({
            id: id,
            newsTitle: "",
            newsText: "",
            author: authors[0]
        })
        if (this.props.addNew) {
            this.props.addNew({
                title: this.state.newsTitle,
                text: this.state.newsText,
                id: this.state.id,
                authors: this.state.author
            });
        }
        
        
    }

    render() {
        return (
            <form id="form" >
                <h1>create post</h1>
                <label className="autor" htmlFor="">
                    Select post author:
                    <select name="" id="" value={this.state.author} onChange={this.setAuthor}>
                        {
                            authors.map((item) => {
                                return (
                                    <option key={item} value={item}>{item}</option>
                                )
                            })
                        }

                    </select>
                </label>
                <label className="NewsForm" >
                    <div className="titleForm">Enter the title:</div>
                    <input 
                        type="text"
                        value={this.state.newsTitle}
                        onChange={this.setNewsTitle} />
                </label>
                <label className="NewsForm" >
                    <div className="titleForm">Enter the text:</div>
                    <textarea
                       
                        type="text"
                        value={this.state.newsText}
                        onChange={this.setNewsText} />
                </label>
               
                <button type="button" onClick={() => { this.addNewPost() }}>create</button>
               
            </form>
        );
    }
}

export default CreateNews;
