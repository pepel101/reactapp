import React from 'react';
import './EditPostNews.css';


const authors = [
    "Egor",
    "Ahmed-ibn-kaktotam",
    "Noname"
]

class EditPostNews extends React.Component {

    constructor(props) {
        super(props)
        this.state = {           
        }
        this.setNewsTitle = this.setNewsTitle.bind(this);
        this.modifiedNewsPost = this.modifiedNewsPost.bind(this);
        this.setAuthor = this.setAuthor.bind(this);
        this.setNewsText = this.setNewsText.bind(this);
    }

    setNewsTitle(event) { //значения из поля ввода измененного заголовка 
        this.setState({
            newsTitle: event.target.value,
        });
    }

    setNewsText(event) { //значения из поля ввода измененного текста 
        this.setState({
            newsText: event.target.value,
        });
    }


    setAuthor(event) { //значения из поля ввода измененного автора
        this.setState({
            author: event.target.value
        })
    }

    
   
    modifiedNewsPost() {  //изменение поста(не работает)
        const id = ++this.state.id;
        this.setState({
            id: id
        })
        if (this.props.selectedPost) {
            this.props.selectedPost({
                title: this.state.newsTitle,
                text: this.state.newsText,
                id: this.state.id,
                authors: this.state.author
            });
        }
    }


    render() {
       
        return (
            <form id="editForm" >
                <label className="autor" htmlFor="">
                    <h1>edit post</h1>
                    Select post author:
                    <select name="" id="" value={this.state.author} onChange={this.setAuthor}>
                        {
                            authors.map((item) => {
                                return (
                                    <option key={item} value={item}>{item}</option>
                                )
                            })
                        }

                    </select>
                </label>
                <label className="NewsForm" >
                    <div className="titleForm">Enter the title:</div>
                    <input
                        type="text"
                        value={this.state.newsTitle}
                        onChange={this.setNewsTitle} />
                </label>
                <label className="NewsForm" >
                    <div className="titleForm">Enter the text:</div>
                    <textarea

                        type="text"
                        value={this.state.newsText}
                        onChange={this.setNewsText} />
                </label>
                <button type="button" onClick={() => {  this.props.save();}} >save</button>
                
            </form>
            
        );
    }
}

export default EditPostNews;