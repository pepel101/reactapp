import React from 'react';
import "./NewsList.css"
import EditPostNews from '../EditPostNews/EditPostNews'


class NewsPost extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            allNews: this.props.allNews || [],
            showEditPost: false,
            addNews: this.props.addNews,
        }
        this.handleEditPost = this.handleEditPost.bind(this);
        this.editPost = this.editPost.bind(this);

    }


    delettePost(i) { //удаление поста
        const arr = this.state.allNews;
        arr.splice(i, 1);
        this.setState({ allNews: arr });

    }
    editPost(editArr, i) {  //внесение измений в пост(не работает) и скрытие формы редактирования
        const arr = this.state.allNews;
        arr[i] = editArr
        this.setState({ 
            allNews: arr,
            showEditPost: false,
         });
    }

    handleEditPost() {  // открывает форму изменения поста
        this.setState({
            showEditPost: true
        })

    }
    

    render() {
        
        return (

            <div>
                {

                    this.state.allNews.map((item,i) => {
                        return <div className="post" key={i} index={i}>
                            <div className="titlePost">{item.title}</div>
                            <div className="textPost">{item.text}</div>
                            <div>post autor: {item.authors}</div>
                            <button type="button" onClick={() => { this.delettePost(item) }}>dellete</button>
                            <button type="button" onClick={() => { this.handleEditPost() }}>edit</button>
                            
                        </div>

                    })
                }
                {
                    this.state.showEditPost && (
                        <EditPostNews
                            selectedPost={this.state.selectedPost}
                            allNews={this.state.allNews}
                            editNews={this.props.updateEditNews}
                            save={this.editPost}
                            addNew={this.props.updateAllNews}
                        />
                    )
                }
            </div >

        )
    }
}
export default NewsPost;